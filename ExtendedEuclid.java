/******************************************************************************
 *  Derleme    :    javac ExtendedEuclid.java
 *  Çalıştırma :    java ExtendedEuclid a b
 *  <p>
 *  Genişletilmiş Öklid algoritmasını kullanarak iki sayının ortak bölenini
 *  doğrusal kombinezonu şeklinde yazar. ( g = t.a + u.b )
 *  </p>
 *  java ExtendedEuclid 55 87
 *  1 = (19).55 + (-12).87
 ******************************************************************************/

public class ExtendedEuclid {

    // gcd(x, y) = g = t.x + u.y
    static int[] gcdext(int x, int y) {
        if (y == 0) {
            // gcd(x, 0) = x = 1.x + 0.y
            return new int[]{x, 1, 0};
        } else {
            int values[] = gcdext(y, x % y);
            int g = values[0];
            int u = values[1] - (x / y) * values[2];
            int t = values[2];
            return new int[]{g, t, u};
        }
    }

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int values[] = gcdext(a, b);

        System.out.printf("%d = (%d).%d + (%d).%d\n", values[0], values[1],
                                                      a, values[2], b);
    }
}
