/******************************************************************************
 * <p>
 * Cipher Texte göre plaintexti Affine Cipher Algoritması ile şifreler.
 * </p>
 * Plaintext : hot
 * Affine Cipher algoritmasıyla şifrelendi...
 * CipherText :axg
 * .....
 * Ciphertext : axg
 * Affine Cipher algoritmasıyla şifre çözülüyor...
 * Plaintext : hot
 ******************************************************************************/

public class AffineCipher {

    static String encrypt(String alphabet, int k[], String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";

        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            int x = alphabet.indexOf(c);

            int e = (k[0] * x + k[1]) % 26; // şifreleme kuralı
            cipherText += alphabet.charAt(e);
        }
        return cipherText;
    }

    static String decrypt(String alphabet, int k[], String cipherText, int inverse) {
        // Şifrelenmiş metini saklamak için
        String plainText = "";

        for (int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            int y = alphabet.indexOf(c);

            int e = (inverse * (y - k[1])) % 26; // şifreleme kuralı
            if (e < 0) {
                e += 26;
            }
            plainText += alphabet.charAt(e);
        }
        return plainText;
    }

    // gcd(x, y) = g = t.x + u.y
    static int[] gcdext(int x, int y) {
        if (y == 0) {
            // gcd(x, 0) = x = 1.x + 0.y
            return new int[]{x, 1, 0};
        } else {
            int values[] = gcdext(y, x % y);
            int g = values[0];
            int u = values[1] - (x / y) * values[2];
            int t = values[2];
            return new int[]{g, t, u};
        }
    }

    public static void main(String[] args) {
        int[] key = {7, 3};
        String plainText = "hot";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        int control[] = gcdext(key[0], 26);
        if (control[0] == 1) {
            System.out.println("Plaintext : " + plainText);
            String cipherText = encrypt(alphabet, key, plainText);
            System.out.println("Affine Cipher algoritmasıyla şifrelendi...");
            System.out.println("CipherText :" + cipherText);

            System.out.println(".....");

            System.out.println("Ciphertext : " + cipherText);
            if (control[1] < 0) {
                control[1] += 26;
            }
            plainText = decrypt(alphabet, key, cipherText, control[1]);
            System.out.println("Affine Cipher algoritmasıyla şifre çözülüyor...");
            System.out.println("Plaintext : " + plainText);
        } else {
            System.out.printf("!! HATALI GİRİŞ : %d ile %d aralarında asal değil..\n", key[0], 26);
        }


    }
}
