/******************************************************************************
 *  Derleme    :    javac ModularInverse.java
 *  Çalıştırma :    java ModularInverse a n
 *  <p>
 *  Komut satırından a ve n değerlerini okur ve eğer varsa a^-1 mod n yi hesaplar
 *  </p>
 *  java ModularInverse 55 87
 *  55 sayısının mod 87 de çarpma işlemine göre tersi : 19
 ******************************************************************************/

class ModularInverse {

    // gcd(x, y) = g = t.x + u.y
    static int[] gcdext(int x, int y) {
        if (y == 0) {
            // gcd(x, 0) = x = 1.x + 0.y
            return new int[]{x, 1, 0};
        } else {
            int values[] = gcdext(y, x % y);
            int g = values[0];
            int u = values[1] - (x / y) * values[2];
            int t = values[2];
            return new int[]{g, t, u};
        }
    }

    static int inverse(int a, int n) {
        int[] values = gcdext(a, n);

        // sayılar aralarında asal değilse tersi yoktur
        if (values[0] > 1) {
            System.out.printf("%d sayısının mod %d de çarpma işlemine göre tersi yoktur!!\n", a, n);
            return 0;
        }

        if (values[1] > 0) {
            return values[1];
        }
        return n + values[1];
    }

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);
        int result = inverse(a, n);
        if (result != 0) {
            System.out.printf("%d sayısının mod %d de çarpma işlemine göre tersi : %d\n", a, n, result);
        }

    }
}