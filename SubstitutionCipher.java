/******************************************************************************
 *  <p>
 *  Cipher Texte göre plaintexti Substitution Cipher Algoritması ile şifreler.
 *  </p>
 *  Plaintext : Aysetatileciksin
 *  Substitution Cipher algoritmasıyla şifrelendi...
 *  CipherText :xivhmxmzbhyzwvzs
 *  .....
 *  Ciphertext : xivhmxmzbhyzwvzs
 *  Substitution Cipher algoritmasıyla şifre çözülüyor...
 *  Plaintext : aysetatileciksin
 ******************************************************************************/

public class SubstitutionCipher {

    static String encrypt(String alphabet, String permutation, String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";

        for(int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            int index = alphabet.indexOf(c);

            cipherText += permutation.charAt(index);
        }
        return cipherText;
    }

    static String decrypt(String alphabet, String permutation, String cipherText) {

        // Şifrelenmiş metini saklamak için
        String plainText = "";

        for(int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            int index = permutation.indexOf(c);

            plainText += alphabet.charAt(index);
        }
        return plainText;
    }

    public static void main(String[] args) {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String permutation = "xnyahpogzqwbtsflrcvmuekjdi";
        String plainText = "Aysetatileciksin";

        System.out.println("Plaintext : " + plainText);
        String cipherText = encrypt(alphabet, permutation, plainText.toLowerCase());
        System.out.println("Substitution Cipher algoritmasıyla şifrelendi...");
        System.out.println("CipherText :" +  cipherText);

        System.out.println(".....");

        System.out.println("Ciphertext : " + cipherText);
        plainText = decrypt(alphabet, permutation, cipherText);
        System.out.println("Substitution Cipher algoritmasıyla şifre çözülüyor...");
        System.out.println("Plaintext : " + plainText);
    }
}
