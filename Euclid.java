/******************************************************************************
 *  Derleme    :    javac Euclid.java
 *  Çalıştırma :    java Euclid a b
 *  <p>
 *  Öklid algoritmasını kullanarak iki sayının obebini bulan program
 *  </p>
 *  java Euclid 252 198
 *  ebob(252, 198) : 18
 ******************************************************************************/

public class Euclid {

    public static long gcd(long a, long b) {
       if(b == 0) {
           return a;
       } else {
           return gcd(b, a % b);
       }
    }

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.out.printf("ebob(%d, %d) : %d\n", a, b, gcd(a, b));
    }
}
