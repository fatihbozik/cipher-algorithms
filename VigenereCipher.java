/******************************************************************************
 * <p>
 * Cipher Texte göre plaintexti Vigenere Cipher Algoritması ile şifreler.
 * </p>
 * Plaintext : thiscryptosystemisnotsecure
 * Vigenere Cipher algoritmasıyla şifrelendi...
 * CipherText :vpxzgiaxivwpubttmjpwizitwzt
 * .....
 * Ciphertext : vpxzgiaxivwpubttmjpwizitwzt
 * Vigenere Cipher algoritmasıyla şifre çözülüyor...
 * Plaintext : thiscryptosystemisnotsecure
 ******************************************************************************/
public class VigenereCipher {

    static String encrypt(String alphabet, int k[], String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";

        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            int x = alphabet.indexOf(c);

            int e = x + k[i % k.length]; // şifreleme kuralı
            e %= 26;
            cipherText += alphabet.charAt(e);
        }
        return cipherText;
    }

    static String decrypt(String alphabet, int k[], String cipherText) {

        // Şifrelenmiş metini saklamak için
        String plainText = "";

        for (int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            int y = alphabet.indexOf(c);

            int d = y - k[i % k.length]; // şifre çözme kuralı
            if(d < 0) { d += 26; }
            plainText += alphabet.charAt(d);
        }
        return plainText;
    }

    public static void main(String[] args) {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String plainText = "thiscryptosystemisnotsecure";
        int key[] = {2, 8, 15, 7, 4, 17};

        System.out.println("Plaintext : " + plainText);
        String cipherText = encrypt(alphabet, key, plainText);
        System.out.println("Vigenere Cipher algoritmasıyla şifrelendi...");
        System.out.println("CipherText :" + cipherText);

        System.out.println(".....");

        System.out.println("Ciphertext : " + cipherText);
        plainText = decrypt(alphabet, key, cipherText);
        System.out.println("Vigenere Cipher algoritmasıyla şifre çözülüyor...");
        System.out.println("Plaintext : " + plainText);
    }
}
