/******************************************************************************
 * <p>
 * Cipher Texte göre plaintexti Permutation Cipher Algoritması ile şifreler.
 * </p>
 * Plaintext : shesellsseashellsbytheseashare
 * Permutation Cipher algoritmasıyla şifrelendi...
 * CipherText :eeslshsalseslshblehsyeethraeas
 * .....
 * Ciphertext : eeslshsalseslshblehsyeethraeas
 * Permutation Cipher algoritmasıyla şifre çözülüyor...
 * Plaintext : eeslshsalseslshblehsyeethraeas
 ******************************************************************************/
public class PermutationCipher {

    static String encrypt(int[] p, String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";
        int startingIndex = 0;

        String[] parts = new String[plainText.length() / p.length];

        for (int i = 0; i < parts.length; i++) {

            parts[i] = plainText.substring(startingIndex, startingIndex + 6);
            startingIndex += 6;
        }

        for (int i = 0; i < parts.length; i++) {
            for (int j = 0; j < parts[i].length(); j++) {
                int index = p[j];
                cipherText += parts[i].charAt(index - 1);
            }
        }
        return cipherText;
    }

    static String decrypt(int[] p, String cipherText) {


        String plainText = "";
        int startingIndex = 0;

        String[] parts = new String[cipherText.length() / p.length];

        for (int i = 0; i < parts.length; i++) {

            parts[i] = cipherText.substring(startingIndex, startingIndex + 6);
            startingIndex += 6;
        }

        for (int i = 0; i < parts.length; i++) {
            for (int j = 0; j < parts[i].length(); j++) {
                int index = p[p[j] - 1];
                plainText += parts[i].charAt(index - 1);
            }
        }
        return cipherText;
    }

    public static void main(String[] args) {
        String plainText = "shesellsseashellsbytheseashare";
        int permutation[] = {3, 5, 1, 6, 4, 2};

        System.out.println("Plaintext : " + plainText);
        String cipherText = encrypt(permutation, plainText);
        System.out.println("Permutation Cipher algoritmasıyla şifrelendi...");
        System.out.println("CipherText :" + cipherText);

        System.out.println(".....");

        System.out.println("Ciphertext : " + cipherText);
        plainText = decrypt(permutation, cipherText);
        System.out.println("Permutation Cipher algoritmasıyla şifre çözülüyor...");
        System.out.println("Plaintext : " + plainText);
    }
}
