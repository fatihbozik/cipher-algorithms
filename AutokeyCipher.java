/******************************************************************************
 * <p>
 * Cipher Texte göre plaintexti Autokey Cipher Algoritması ile şifreler.
 * </p>
 * Plaintext : rendezvous
 * AutoKey Cipher algoritmasıyla şifrelendi...
 * CipherText :zvrqhdujim
 * .....
 * Ciphertext : zvrqhdujim
 * AutoKey Cipher algoritmasıyla şifre çözülüyor...
 * Plaintext : rendezvous
 ******************************************************************************/

public class AutokeyCipher {

    static String encrypt(String alphabet, int[] z, String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";

        for(int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            int x = alphabet.indexOf(c);

            if(i + 1 != plainText.length()) {
                z[i + 1]  = x;
            }
            int e = (x + z[i]) % 26;

            cipherText += alphabet.charAt(e);
        }
        return cipherText;
    }

    static String decrypt(String alphabet, int[] z, String cipherText) {

        // Şifrelenmiş metini saklamak için
        String plainText = "";

        for(int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            int y = alphabet.indexOf(c);

            int e = (y - z[i]) % 26;

            if(e < 0) e += 26;

            plainText += alphabet.charAt(e);
        }
        return plainText;
    }

    public static void main(String[] args) {
        String plainText = "rendezvous";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int k = 8;
        int z[] = new int[plainText.length()];
        z[0] = k;

        System.out.println("Plaintext : " + plainText);
        String cipherText = encrypt(alphabet, z, plainText);
        System.out.println("AutoKey Cipher algoritmasıyla şifrelendi...");
        System.out.println("CipherText :" + cipherText);

        System.out.println(".....");

        System.out.println("Ciphertext : " + cipherText);
        plainText = decrypt(alphabet, z, cipherText);
        System.out.println("AutoKey Cipher algoritmasıyla şifre çözülüyor...");
        System.out.println("Plaintext : " + plainText);




    }
}
