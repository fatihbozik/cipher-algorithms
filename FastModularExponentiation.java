/******************************************************************************
 * Derleme    :    javac FastModularExponentiation.java
 * Çalıştırma :    java FastModularExponentiation a b m
 * <p>
 * Hızı modüler üs alma algoritmasını kullanarak
 * a^b mod m sonucunu hesaplayacak.
 * </p>
 * java FastModularExponentiation 7 560 561
 * 7^(560) = 1 mod 561
 ******************************************************************************/

public class FastModularExponentiation {

    static int modulerExponentiation(int a, int b, int m) {
        int c = 0;  // bitiminde b'ye eşit olacak
        int d = 1;  // bitiminde a^(b)'ye eşit olacak

        String str_b = Integer.toBinaryString(b);

        for (int i = 0; i <= str_b.length() - 1; i++) {
            c *= 2;
            d = (d * d) % m;

            if (str_b.charAt(i) == '1') {
                c++;
                d = (d * a) % m;
            }
        }
        return d;
    }


    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int m = Integer.parseInt(args[2]);

        int result = modulerExponentiation(a, b, m);

        System.out.printf("%d^(%d) = %d mod %d\n", a, b, result, m);
    }
}
