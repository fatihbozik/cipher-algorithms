/******************************************************************************
 *  Derleme    :    javac LinearEquation.java
 *  Çalıştırma :    java LinearEquation a b m
 *  <p>
 *  a.x = b mod m, eşitliğini sağlayan x sayısını bulan program
 *  </p>
 *  java LinearEquation 3 4 7
 *  3.x = 4 (mod 7), gcd(3, 7) = 1
 *  x = 6
 *
 *  java LinearEquation 28 14 21
 *  28.x = 14 (mod 21), gcd(28, 21) = 7
 *  4.x = 2 (mod 3), gcd(4, 3) = 1
 *  x = 2
 ******************************************************************************/

public class LinearEquation {

    // gcd(x, y) = g = t.x + u.y
    static int[] gcdext(int x, int y) {
        if (y == 0) {
            // gcd(x, 0) = x = 1.x + 0.y
            return new int[]{x, 1, 0};
        } else {
            int values[] = gcdext(y, x % y);
            int g = values[0];
            int u = values[1] - (x / y) * values[2];
            int t = values[2];
            return new int[]{g, t, u};
        }
    }

    static int inverse(int a, int n) {
        int[] values = gcdext(a, n);

        // sayılar aralarında asal değilse tersi yoktur
        if (values[0] > 1) {
            System.out.printf("%d sayısının mod %d de çarpma işlemine göre tersi yoktur!!\n", a, n);
            return 0;
        }

        if (values[1] > 0) {
            return values[1];
        }
        return n + values[1];
    }


    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int m = Integer.parseInt(args[2]);
        int values[] = gcdext(a, m);

        if(values[0] != 1 && b % values[0] == 0) {
            a /= values[0];
            b /= values[0];
            m /= values[0];
        }
        int result = (inverse(a, m) * b) % m;
        if(result > 0) {
            System.out.printf("%d.x = %d (mod %d), gcd(%d, %d) = %d\n", a * values[0], b * values[0] , m * values[0],
                                                                        a * values[0], m * values[0], values[0]);
            System.out.printf("%d.x = %d (mod %d), gcd(%d, %d) = 1\n", a, b, m, a, m);
            System.out.printf("x = %d\n", result);
        }

    }
}
