/******************************************************************************
 *  <p>
 *  Cipher Texte göre plaintexti Shift Cipher Algoritması ile şifreler.
 *  </p>
 *  Plaintext : wewillmeetatmidnight
 *  Shift Cipher algoritmasıyla şifrelendi...
 *  CipherText :hphtwwxppelextoxtrse
 *  .....
 *  Ciphertext : hphtwwxppelextoxtrse
 *  Shift Cipher algoritmasıyla şifre çözülüyor...
 *  Plaintext : wewillmeetatmidnight
 ******************************************************************************/

public class ShiftCipher {

    static String encrypt(String alphabet, int k, String plainText) {

        // Şifrelenmiş metini saklamak için
        String cipherText = "";

        for(int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            int x = alphabet.indexOf(c);

            int e = (x + k) % 26; // şifreleme kuralı

            cipherText += alphabet.charAt(e);
        }
        return cipherText;
    }

    static String decrypt(String alphabet, int k, String cipherText) {
        // Şifrelenmiş metini saklamak için
        String plaintext = "";

        for(int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            int y = alphabet.indexOf(c);

            int d = (y - k) % 26; // şifreleme kuralı
            if(d < 0) { d += 26; }
            plaintext += alphabet.charAt(d);
        }
        return plaintext;
    }

    public static void main(String[] args) {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String plainText = "wewillmeetatmidnight";
        int key = 11;

        System.out.println("Plaintext : " + plainText);
        String cipherText = encrypt(alphabet, 11, plainText);
        System.out.println("Shift Cipher algoritmasıyla şifrelendi...");
        System.out.println("CipherText :" +  cipherText);

        System.out.println(".....");

        System.out.println("Ciphertext : " + cipherText);
        plainText = decrypt(alphabet, 11, cipherText);
        System.out.println("Shift Cipher algoritmasıyla şifre çözülüyor...");
        System.out.println("Plaintext : " + plainText);
    }
}
