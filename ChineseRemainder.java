/******************************************************************************
 *  Derleme    :    javac ChineseRemainder.java
 *  Çalıştırma :    java ChineseRemainder a1 b1 m1 a2 b2 m2 a3 b3 m3
 *  <p>
 *  Çinli kalanlar teoremini uygulayan program
 *  a1.x = b1 mod m1
 *  a2.x = b2 mod m2
 *  a3.x = b3 mod m3
 *  x = ?
 *  </p>
 *  java ChineseRemainder 15 21 48 166 46 22 1 5 13
 *  15x = 21 mod 48
 *  166x = 46 mod 22
 *  1x = 5 mod 13
 *
 *  ....
 *
 *   x : 2059
 ******************************************************************************/

public class ChineseRemainder {

    //region Doğrusal Denklem Çözümü için
    // gcd(x, y) = g = t.x + u.y
    static int[] gcdext(int x, int y) {
        if (y == 0) {
            // gcd(x, 0) = x = 1.x + 0.y
            return new int[]{x, 1, 0};
        } else {
            int values[] = gcdext(y, x % y);
            int g = values[0];
            int u = values[1] - (x / y) * values[2];
            int t = values[2];
            return new int[]{g, t, u};
        }
    }

    static int inverse(int a, int n) {
        int[] values = gcdext(a, n);

        // sayılar aralarında asal değilse tersi yoktur
        if (values[0] > 1) {
            System.out.printf("%d sayısının mod %d de çarpma işlemine göre tersi yoktur!!\n", a, n);
            return 0;
        }

        if (values[1] > 0) {
            return values[1];
        }
        return n + values[1];
    }

    static int[] solveEquation(int a, int b, int m) {
        int values[] = gcdext(a, m);

        if (values[0] != 1 && b % values[0] == 0) {
            a /= values[0];
            b /= values[0];
            m /= values[0];
        }

        int result = (inverse(a, m) * b) % m;
        return new int[]{result, m};
    }
    //endregion

    public static void main(String[] args) {

        if (args.length % 3 != 0) {
            System.out.println("Yanlış sayıda paramtre girdiniz..");
        } else {
            int array_a[] = new int[args.length / 3];
            int array_b[] = new int[args.length / 3];
            int array_m[] = new int[args.length / 3];

            for (int i = 0; i < args.length; i++) {
                int parameter = Integer.parseInt(args[i]);
                if (i % 3 == 0) {
                    array_a[i / 3] = parameter;
                } else if (i % 3 == 1) {
                    array_b[i / 3] = parameter;
                } else {
                    array_m[i / 3] = parameter;
                }
            }

            for (int i = 1; i <= args.length / 3; i++) {
                System.out.printf("%dx = %d mod %d\n", array_a[i - 1], array_b[i - 1], array_m[i - 1]);
                int temp[] = solveEquation(array_a[i - 1], array_b[i - 1], array_m[i - 1]);
                array_b[i - 1] = temp[0];
                array_m[i - 1] = temp[1];
            }

            int m = 1;
            for(int i = 0; i < array_b.length; i++) {
                m *= array_m[i];
            }

            int array_M[] = new int[array_m.length];
            for(int i = 0; i < array_M.length; i++) {
                array_M[i] = m / array_m[i];
            }

            int array_y[] = new int[array_m.length];
            for(int i = 0; i < array_y.length; i++) {
                array_y[i] = inverse(array_M[i], array_m[i]);
            }

            int x = 0;
            for(int i = 0; i < array_y.length; i++) {
                x += array_b[i] * array_M[i] * array_y[i];
            }
            x %= m;

            System.out.println("\n....");
            System.out.println("\nx : " + x);
        }
    }
}
